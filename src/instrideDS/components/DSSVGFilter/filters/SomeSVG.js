
// ATTENTION: preserveAspectRatio="none" is required to make the SVG responsive, always add it

// The original opacities were set to 0.04 and 0.05, that seems really low, we need to confirm with design if that is the actual value. 
//They had this agaisnt a pure white BG so it seems like it could be a mistake.

export const svgString = (color, backgroundColor) => `
<svg width="834" height="818" viewBox="0 0 834 818" preserveAspectRatio="none" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0_14901_42903)">
    <path d="M833.5 0H-155.5V818H833.5V0Z" fill="none"/>
    <path opacity="0.03" d="M833.5 0H-155.5V814.282C-155.5 814.282 70.459 675.965 423.477 739.377C669.463 783.563 833.5 730.389 833.5 730.389V0Z" fill="${backgroundColor || "white"}"/>
    <path opacity="0.4" d="M-155.5 818C470.615 818 764.715 578.549 833.5 458.824V818H-155.5Z" fill="${color}"/>
    <path opacity="0.5" d="M-155.5 818C470.615 818 764.715 685.754 833.5 619.449V818H-155.5Z" fill="${color}"/>
  </g>
  <defs>
    <linearGradient id="paint0_linear_14901_42903" x1="832.813" y1="3.71833" x2="-264.712" y2="196.615" gradientUnits="userSpaceOnUse">
      <stop stop-color="white"/>
      <stop offset="1" stop-color="${color}"/>
    </linearGradient>
    <clipPath id="clip0_14901_42903">
      <rect width="834" height="818" fill="white"/>
    </clipPath>
  </defs>
</svg>`;

