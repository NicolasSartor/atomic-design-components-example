import styled from 'styled-components';
import {svgString} from './filters/SomeSVG'
 
const SVGWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 2;

  svg {
    width: 100%;
    height: 100%;
  }
`;

const ImageWrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 400px; // You can adjust this
  img {
    width: 100%;
    height: auto;
  }
`;

const DSSVGFilter = ({ ImageComponent, color }) => {
  return (
    <ImageWrapper>
      {ImageComponent}
      <SVGWrapper dangerouslySetInnerHTML={{ __html: svgString(color) }} />
    </ImageWrapper>
  );
};

export default DSSVGFilter;
