import styled from 'styled-components';

const Typography = styled.p`
  font-size: ${props => props.size || '16px'};
  color: ${props => props.color || 'black'};
  font-weight: ${props => props.weight || 'normal'};
`;

export default Typography;
