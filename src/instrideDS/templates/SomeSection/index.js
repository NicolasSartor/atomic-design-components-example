import React from 'react';
import DSSVGFilter from '../../components/DSSVGFilter';
import DSTypography from '../../components/DSTypography'


const DesktopVariant = ({ImageComponent, title, contentSubTitle, description, onClick}) => {
  return (
    <div style={{border:"1px solid red"}}>
      <DSTypography size="24px" weight="700">{title}</DSTypography>
      <div id="image-wrapper">
        <DSSVGFilter ImageComponent={ImageComponent} color="blue"/>
      </div>
      <div>
        <DSTypography>{contentSubTitle}</DSTypography>
        <DSTypography>{description}</DSTypography>
        <button onClick={onClick}>Do Something</button>
      </div>
    </div>
  );
}

const MobileVariant = ({ImageComponent, title, contentSubTitle, description, onClick}) => {
  return (
    <div style={{border:"1px solid red"}}>
      <DSTypography size="20px" weight="700">{title} Mobile</DSTypography>
      <div id="image-wrapper">
        <DSSVGFilter ImageComponent={ImageComponent} color="red"/>
      </div>
      <div>
        <DSTypography>{contentSubTitle}</DSTypography>
        <DSTypography>{description}</DSTypography>
        <button onClick={onClick}>Do Something</button>
      </div>
    </div>
  );
}

const SomeSection = ({variant, ...rest}) => {

  if(variant==="desktop"){
    return (<DesktopVariant {...rest}/>)
  }
  if(variant==="mobile"){
    return (<MobileVariant {...rest}/>)
  }
}

export default SomeSection;
