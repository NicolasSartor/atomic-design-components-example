export const getSomePageData = (cpId) => {
  return{
    someSectionData:{
        title:"Some Section Title",
        contentSubtitle:"Some Content Subtitle/title",
        description:"This is the description of this section.",
        imgSrc:"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Cadiz_Wilderness_and_Valley.jpg/1920px-Cadiz_Wilderness_and_Valley.jpg"
    }
  }
}