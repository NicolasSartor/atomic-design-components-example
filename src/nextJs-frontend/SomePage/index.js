import React from 'react';
import SomeSection from '../../instrideDS/templates/SomeSection'
import {getSomePageData} from './data'

const SomePage = () => {
  const businessLogic = () => alert('some business logic')

  const variantLogic = () => {
    const isMobileLogic = () => false;

    if(isMobileLogic()){
      return "mobile"
    }else{
      return "desktop"
    }
  };

  // This would come from BFF - If not available keep regular data layer
  const {someSectionData} = getSomePageData("cpId");

  const OptmizedImageComponent = <img src={someSectionData.imgSrc} alt="Background" style={{ width: '100%' }} />;

  return (
    <div>
      <header>
        <h1>SomePage</h1>
      </header>
      <main>
        {/* Page Layouts are defined at page level, do not give sections spacing props */}
        <div style={{margin: "50px"}}>
          {/* Content is always passed down, as well as any business logic */}
          <SomeSection 
            ImageComponent={OptmizedImageComponent} 
            title={someSectionData.title}
            contentSubTitle={someSectionData.contentSubTitle}
            description={someSectionData.description}
            onClick={businessLogic}
            variant={variantLogic()}
          />
        </div>
      </main>
    </div>
  );
}

export default SomePage;
